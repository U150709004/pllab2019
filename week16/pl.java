import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

public class pl {

    static LinkedList<String> linkList = new LinkedList<>();
    static Set<String> setList = new HashSet<>();
    static Heap set;
    static PriorityQueue<String> prioritySet = new PriorityQueue<>();
	static String url = "/root/Desktop/names.txt";

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Linked List fill time: " + linkListFiller() + " ns");
        System.out.println("HashSet fill time: " + hashSetListFiller() + " ns");
        System.out.println("Heap fill time: " + heapFiller() + " ns");
        System.out.println("Priority Queue fill time: " + priorityFiller() + " ns");
    }

    private static Long linkListFiller() throws FileNotFoundException{
        Scanner scanner = new Scanner(new FileReader(url));
        Long startTime = System.nanoTime();
        while (scanner.hasNext()){
            linkList.add(scanner.next());
        }
        Long endTime = System.nanoTime();
        scanner.close();
        return (endTime - startTime) ;
    }

    private static Long hashSetListFiller() throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(url));
        Long startTime = System.nanoTime();
        while (scanner.hasNext()){
            setList.add(scanner.next());
        }
        Long endTime = System.nanoTime();
        scanner.close();
        return (endTime - startTime) ;
    }

    private static Long heapFiller() throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(url));
        int size = 0;
        while (scanner.hasNext()){
            size = size + 1;
            scanner.next();
        }
        set = new Heap(size);
        scanner = new Scanner(new FileReader(url));
        Long startTime = System.nanoTime();
        while (scanner.hasNext()){
            set.insert(scanner.next());
        }
        Long endTime = System.nanoTime();
        scanner.close();
        return (endTime - startTime) ;
    }

    private static Long priorityFiller() throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(url));
        Long startTime = System.nanoTime();
        while (scanner.hasNext()){
            prioritySet.add(scanner.next());
        }
        Long endTime = System.nanoTime();
        scanner.close();
        return (endTime - startTime) ;
    }

    static class Heap{

        private static final int d = 2;
        private int heapSize;
        private String[] value;

        public Heap(int capacity)
        {
            heapSize = 0;
            value = new String[capacity + 1];
        }

        public boolean isEmpty( )
        {
            return heapSize == 0;
        }

        public boolean isFull( )
        {
            return heapSize == value.length;
        }

        private int parent(int i)
        {
            return (i - 1)/d;
        }
        private int kthChild(int i, int k)
        {
            return d * i + k;
        }

        public void insert(String x){
            if (isFull( ) )
                throw new NoSuchElementException("Overflow Exception");

            value[heapSize++] = x;
            heapifyUp(heapSize - 1);
        }

        public String delete(int ind)
        {
            if (isEmpty() )
                throw new NoSuchElementException("Underflow Exception");
            String keyItem = value[ind];
            value[ind] = value[heapSize - 1];
            heapSize--;
            heapifyDown(ind);
            return keyItem;
        }

        private void heapifyUp(int childInd)
        {
            String tmp = value[childInd];
            while (childInd > 0 && tmp.length() < value[parent(childInd)].length())
            {
                value[childInd] = value[ parent(childInd) ];
                childInd = parent(childInd);
            }
            value[childInd] = tmp;
        }

        private void heapifyDown(int ind)
        {
            int child;
            String tmp = value[ ind ];
            while (kthChild(ind, 1) < heapSize)
            {
                child = minChild(ind);
                if (value[child].length() < tmp.length())
                    value[ind] = value[child];
                else
                    break;
                ind = child;
            }
            value[ind] = tmp;
        }

        private int minChild(int ind)
        {
            int bestChild = kthChild(ind, 1);
            int k = 2;
            int pos = kthChild(ind, k);
            while ((k <= d) && (pos < heapSize))
            {
                if (value[pos].length() < value[bestChild].length())
                    bestChild = pos;
                pos = kthChild(ind, k++);
            }
            return bestChild;
        }

        public void printHeap()
        {
            System.out.print("[");
            for (int i = 0; i < heapSize; i++){
                if(i == heapSize - 1)
                    System.out.print(value[i] +"]");
                else
                    System.out.print(value[i] +", ");
            }
        }
    }

}
