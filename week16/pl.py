import time
names = open("names.txt", "r")
nameList = names.read().split("\n")


### Set Data Structure
setDataStructure = set()
start = time.time()
for line in nameList:
  setDataStructure.add(line[:-2])
print("Set took ",time.time() - start)

### Heap Data Structure
def heapify(arr, n, i): 
    largest = i
    l = 2 * i + 1
    r = 2 * i + 2
    if l < n and arr[i] < arr[l]: 
        largest = l 
    if r < n and arr[largest] < arr[r]: 
        largest = r 
    if largest != i: 
        arr[i],arr[largest] = arr[largest],arr[i]
        heapify(arr, n, largest) 

def heapSort(arr): 
    n = len(arr) 
    for i in range(n, -1, -1): 
        heapify(arr, n, i) 
    for i in range(n-1, 0, -1): 
        arr[i], arr[0] = arr[0], arr[i]
        heapify(arr, i, 0)
heapDataStructure = nameList
start = time.time()
heapSort(heapDataStructure)
print("Heap took ",time.time() - start)

### Linked List Data Structure
class Node(object):
    def __init__(self, data=None, next_node=None):
        self.data = data
        self.next_node = next_node
    def get_data(self):
        return self.data
    def get_next(self):
        return self.next_node
    def set_next(self, new_next):
        self.next_node = new_next

class LinkedList(object):
    def __init__(self, head=None):
        self.head = head
    def insert(self, data):
        new_node = Node(data)
        new_node.set_next(self.head)
        self.head = new_node
    def size(self):
        current = self.head
        count = 0
        while current:
            count += 1
            current = current.get_next()
        return count

linkedListData = LinkedList()
start = time.time()
for name in nameList:
  linkedListData.insert(name)
print("Linked List took ",time.time() - start)


